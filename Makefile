run:
	cargo run

build:
	cargo build

release:
	cargo build --release

clean:
	rm -rf target