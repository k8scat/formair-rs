extern crate imap;
extern crate native_tls;

use imap::types::{Fetch, ZeroCopy};

#[derive(Debug)]
pub struct IMAP {
    pub host: String,
    pub port: u16,
    pub username: String,
    pub password: String,
}

impl IMAP {
    pub fn new(host: String, port: u16, username: String, password: String) -> IMAP {
        IMAP {
            host,
            port,
            username,
            password,
        }
    }

    pub fn list_messages(
        &self,
        sequence_set: &str,
    ) -> imap::error::Result<Option<ZeroCopy<Vec<Fetch>>>> {
        let tls = native_tls::TlsConnector::builder().build()?;
        let port = self.port.to_usize();
        let client =
            imap::connect((self.host.as_str(), port as u16), self.host.as_str(), &tls).unwrap();

        let mut imap_session = client
            .login(self.username.as_str(), self.password.as_str())
            .map_err(|e| e.0)?;
        imap_session.select("INBOX")?;

        let messages = imap_session.fetch(sequence_set, "ALL")?;
        imap_session.logout()?;
        Ok(Some(messages))
    }
}
