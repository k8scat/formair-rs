extern crate openssl;
extern crate rs_pop3;

use openssl::ssl::{SslConnector, SslMethod};
use rs_pop3::POP3Result::{POP3Err, POP3List, POP3Message, POP3Stat};
use rs_pop3::POP3Stream;

#[derive(Debug)]
pub struct POP3 {
    pub host: String,
    pub port: u16,
    pub username: String,
    pub password: String,
}


impl POP3 {
    pub fn new<S>(host: S, port: u16, username: S, password: S) -> POP3
        where S: AsRef<str> {
        POP3 {
            host,
            port,
            username,
            password,
        }
    }

    pub fn list_emails(&self) {
        let mut s = match POP3Stream::connect(
            (self.host.as_str(), self.port),
            Some(SslConnector::builder(SslMethod::tls()).unwrap().build()),
            self.host.as_str(),
        ) {
            Ok(s) => s,
            Err(e) => panic!("{}", e),
        };

        let res = s.login(self.username.as_str(), self.password.as_str());
        match res {
            POP3Err => println!("Err logging in"),
            _ => (),
        }

        let stat = s.stat();
        match stat {
            POP3Stat {
                num_email,
                mailbox_size,
            } => println!("num_email: {},  mailbox_size:{}", num_email, mailbox_size),
            _ => println!("Err for stat"),
        }

        let list_all = s.list(None);
        match list_all {
            POP3List { emails_metadata } => {
                println!("messages len: {}", emails_metadata.len());
            }
            _ => println!("Err for list_all"),
        }

        let message_25 = s.retr(25);
        match message_25 {
            POP3Message { raw } => {
                for i in raw.iter() {
                    println!("{}", i);
                }
            }
            _ => println!("Error for message_25"),
        }
        s.quit();
    }
}
