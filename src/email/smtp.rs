use std::borrow::Borrow;

use lettre::{Message, SmtpTransport, Transport};
use lettre::transport::smtp::authentication::Credentials;
use lettre::transport::smtp::Error;

#[derive(Debug)]
pub struct SMTP {
    pub name: String,
    pub username: String,
    pub password: String,
    pub host: String,
    pub port: u16,
}

impl SMTP {
    pub fn new(name: String, username: String, password: String, host: String, port: u16) -> SMTP {
        SMTP {
            name,
            username,
            password,
            host,
            port,
        }
    }

    pub fn send_email(
        &self,
        recipients: Vec<String>,
        subject: String,
        body: String,
    ) -> Option<Error> {
        let from = format!("{} <{}>", self.name, self.username).parse()?;
        let mut email = Message::builder()
            .from(from)
            .subject(subject);
        for r in recipients.iter() {
            let r = r.parse()?;
            email = email.to(r);
        }
        let email = email.body(body)?;

        let creds = Credentials::new(self.username.to_string(), self.password.to_string());
        let mut mailer = SmtpTransport::relay(self.host.as_str())?;
        let port = self.port.to_usize();
        let mut mailer = mailer.port(port as u16)
            .credentials(creds)
            .build();

        match mailer.send(&email) {
            Ok(_) => None,
            Err(e) => Some(e),
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
