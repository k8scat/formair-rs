pub mod email;

use email::imap;
use rfc2047_decoder;

fn main() {
    let i = imap::IMAP::new(String::from(""), 995, String::from(""), String::from(""));
    let result = i.list_messages("1:10");
    match result {
        Err(e) => println!("err: {:?}", e),
        Ok(o) => {
            if let Some(messages) = o {
                assert_eq!(messages.len(), 10);
                let m = messages.get(1).unwrap();
                // let b = m.body().unwrap();
                // let b = std::str::from_utf8(b).unwrap();
                let env = m.envelope().unwrap();
                let from = env.from.as_ref().unwrap()[0].host.unwrap();
                let from = std::str::from_utf8(from).unwrap();
                let from = rfc2047_decoder::decode(from.as_bytes()).unwrap();
                println!("{}", from);
                assert_eq!(m.message.to_usize(), 1)
            } else {
                println!("no msg")
            }
        }
    }
}
